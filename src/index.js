import './style.scss';
import '@fortawesome/fontawesome-free/js/all';

import i18next from 'i18next';
import AOS from 'aos';
import Moment from 'moment';
import SmoothScroll from 'smooth-scroll';
var scroll = new SmoothScroll('a[href*="#"]', { header: '#pp-navbar', updateURL: false, speed: 100 });
// dev
// var portalSignIn = 'http://localhost:4200/commons';
// var portalRegister = 'http://localhost:4200/commons/register';
//prod

var showModal = false;
var portalSignIn = 'https://portal.paga.pe/commons';
var portalRegister = 'https://portal.paga.pe/commons/register';

// images
import logoImage from './images/logo-pro.png';
import logoImageWhite from './images/logo-pro-white.png';
import logoImageGrey from './images/logo-pro-grey.png';
import logoSquare from './images/logo-square.png';
import logoMovil from './images/pagape-logo3.png'
import logoOlympusGrey from './images/logo-olympus-grey.png';
import logoCustomer from './images/logo-customer.png';
import popupImg from './images/popup.png';

import aboutImg1 from './images/pagape-logo-footer.png'
import contactPhone1 from './images/contact-phone-1.png';

import titleLine from './images/title-line.png';

import registerStep1Img1 from './images/register-step1-img1.png';
import registerStep1Img2 from './images/register-step1-img2.png';
import registerStep1Img3 from './images/register-step1-img3.png';
import registerStep1Num1 from './images/register-step1-num1.png';

import registerStep2Img1 from './images/register-step2-img1.png';
import registerStep2Img2 from './images/register-step2-img2.png';
import registerStep2Img3 from './images/register-step2-img3.png';
import registerStep2Num1 from './images/register-step2-num1.png';

import registerStep3Img1 from './images/register-step3-img1.png';
import registerStep3Img2 from './images/register-step3-img2.png';
import registerStep3Img3 from './images/register-step3-img3.png';
import registerStep3Num1 from './images/register-step3-num1.png';

// import contactImg1 from './images/background-blue.png';
// import contactImg2 from './images/background-blue.png';
// import contactImg3 from './images/background-blue.png';
// import contactImg4 from './images/background-blue.png';
// import contactImg5 from './images/background-blue.png';
// import contactImg6 from './images/background-blue.png';
// import contactImg7 from './images/background-blue.png';
// import contactImg8 from './images/background-blue.png';
import contactAward1 from './images/contact-award1.png';
import contactAward2 from './images/contact-award2.png';
import contactItem1Img1 from './images/bodyHelpSms.png';
import contactItem1Img2 from './images/bodyHelpPhone.png';
import contactItem1Img3 from './images/bodyHelpMail.png';
import contactItem1Img4 from './images/bodyHelp.png';

import faqImg1 from './images/faq-01.png';
import faqImg2 from './images/faq-02.png';
import faqImg3 from './images/faq-03.png';
import faqImg4 from './images/faq-04.png';
import faqImg5 from './images/faq-05.png';

var homeTemplate = document.querySelector('#home');
var faqTemplate = document.querySelector('#faq');
// i18n 
Moment.locale('es');
i18next.init({
    lng: 'es',
    debug: true,
    resources: {
        es: {
            translation: {
                'nav-home': 'Inicio',
                'nav-about-us': 'Nosotros',
                'nav-register': 'Regístrate',
                'nav-pricing': 'Costos',
                'nav-contact': 'Contacto',
                'nav-pros': 'Beneficios',
                'button-login': 'Iniciar sesión',
                'button-register': 'Regístrate',
                'button-start': 'Abre tu cuenta y recibe pagos',
                'button-freetrial': 'Empieza a cobrar',
                'button-go': 'Saber más',
                'button-more': 'Más información',

                'home': {
                    'title': 'Bienvenido a',
                    'subtitle1': 'Tu plataforma de pagos electrónicos',
                    'subtitle2': 'Escala tu negocio rápido y seguro',
                    'itemtitle': 'Regístrate y empieza a aprovechar los siguientes beneficios:',
                    'item1-1': 'Acepta pagos con tarjeta - ',
                    'item1-2': 'Débito y Crédito',
                    'item2-1': 'Dinero seguro - ',
                    'item2-2': 'Convierte tus pagos en efectivo hasta en 48 horas',
                    'item3-1': 'Administra tus ingresos - ',
                    'item3-2': 'Verifica cuanto están creciendo tus ventas',
                    'footer': {
                        'title1': 'Es una solución de:',
                        'title2': 'Estas empresas confían en nuestras soluciones:',
                    },
                    'movil': {
                        'pagape': 'PAGA.PE',
                        'ago1': '1m ago',
                        'ago2': '8m ago',
                        'msg1': 'ANA FLORES TE PAGO S/ 10.00',
                        'msg2': 'JULIO PAZ TE PAGO S/ 100.00'
                    }
                },
                'pro': {
                    'title': '¿Cómo te beneficias?',
                    'subtitle': 'Lo explicamos rápido, claro y sin floro',
                    'step1': {
                        'title1': 'Recibe pagos con tarjeta',
                        'detail2': '¿Tus clientes quieren pagar con tarjeta?',
                        'detail3': 'Sólo regístrate, y empieza a recibir pagos sin costos de suscripción',
                    },
                    'step2': {
                        'title1': 'Dispón del efectivo',
                        'detail2': '¿Vender a crédito recibiendo el efectivo?',
                        'detail3': 'Tus clientes pagan a crédito y tu recibes el efectivo en tu cuenta',
                    },
                    'step3': {
                        'title1': 'Administra tus ingresos',
                        'detail2': '¿Quieres monitorear tus ventas?',
                        'detail3': 'En tiempo real, verás tu contabilidad de ingresos y cuentas por cobrar',
                    }
                },
                'about': {
                    'title': '¿Cómo funciona?',
                    'subtitle': 'Paso a paso',
                    'text1': '<span class="has-text-weight-bold">Regístrate</span> ingresa tus datos y cuentas en soles y dólares',
                    'text2': '<span class="has-text-weight-bold">Comparte y recibe pagos</span> creando y envía tus enlaces de pagos por todas tus redes',
                    'text3': '<span class="has-text-weight-bold">Monitorea tus ingresos</span> manten orden y seguimiento a tus cobranzas',
                    'text4': '<span class="has-text-weight-bold">Contabilidad</span> de tus ingresos en tiempo real',
                    'detail1': '<span class="has-text-weight-bold">Tu plataforma</span> electrónica para recibir pagos'
                },
                'register': {
                    'title': '¿Cómo empezamos?',
                    'subtitle': 'Paso a paso',
                    'step1': {
                        'title1': 'Crea tu cuenta',
                        'detail1': 'Ingresa tus datos personales e indica tu cuenta de banco.',
                    },
                    'step2': {
                        'title1': 'Empieza a cobrar',
                        'detail1': 'Creando y compartiendo tu enlace de pago en todas tus redes.',
                    },
                    'step3': {
                        'title1': 'Monitorea tus ingresos',
                        'detail1': 'Manten orden y seguimiento a tus cobranzas y contabiliza tus ingresos en tiempo real.',
                    }
                },

                'pricing': {
                    'title': '¿Cuál es el costo?',
                    'subtitle': 'Sólo si tú ganas, nosotros ganamos, sino no te cuesta nada.',
                    'item1': {
                        'detail1': 'S/. 0',
                        'detail2': 'Costo',
                        'detail3': 'Inicial',
                        'detail4': '(cero costo de instalación)',
                        'title': 'Costo Inicial',
                        'subitem1': 'No requieres dispositivos',
                        'subitem2': 'No necesitas integración',
                        'subitem3': 'No tienes que pagar inicial',
                        'subitem4': 'No necesitas web',
                        'subitem5': 'No requieres app',
                        'subitem6': 'No tienes que tener nada'
                    },
                    'item2': {
                        'detail1': 'S/. 0',
                        'detail2': 'Costo',
                        'detail3': 'Mensual',
                        'detail4': '(¡dile no a las suscripciones!)',
                        'title': 'Costo Mensual',
                        'subitem1': 'Sin costos fijos',
                        'subitem2': 'Sin pagos mínimos',
                        'subitem3': 'Sin costos de cancelación',
                        'subitem4': 'Sin costos de mantenimiento',
                        'subitem5': 'Sin costos de renovación'
                    },
                    'item3': {
                        'detail1': '0.00% + IGV',
                        'detail2': '(única comisión)',
                        'title': 'Comisión',
                        'subitem1': 'Sólo si vendes',
                        'subitem2': 'Incluido en la transacción',
                        'subitem3': 'Transparencia en montos',
                        'subitem4': 'Único costo',
                    }
                },
                'contact': {
                    'item2': {
                        'title1': 'Es una solución de:'
                    },
                    'item3': {
                        'title1': 'Nuestra trayectoria:',
                        'detail1': '<strong>25 años de experiencia</strong> en desarrollo de software especializado. Somos una empresa peruana <strong>posicionada</strong> en el mercado local asi como en <strong>Bolivia, México, Canadá, y Estados Unidos.</strong>',
                        'title2': 'Reconocidos y premiados por:'
                    },
                    'title': '¿Quiénes somos?',
                    'subtitle': 'Contáctanos si tienes alguna duda'
                },
                'faq': {
                    'accordion1': {
                        'title': 'Sobre Paga.pe',
                        'question1': '1.1. ¿Qué es Paga.pe?',
                        'answer1': 'Es una pasarela de pagos que llegó para facilitar tus transacciones digitales mediante links o QRs, de forma segura y confiable; además, puedes enviar los enlaces a través de correo electrónico, redes sociales, SMS, entre otros. Cuenta con el respaldo de Olympus Systems.',
                        'question2': '1.2. ¿Cómo me beneficia?',
                        'answer2': 'answer2'
                    },
                    'accordion2': {
                        'title': 'Registros y Afiliaciones',
                        'question1': '2.1. ¿Cómo me registro?',
                        'answer1': 'answer1',
                        'question2': '2.2. ¿Qué requisitos debo tener para afiliarme?',
                        'answer2': 'answer2'
                    },
                    'accordion3': {
                        'title': 'Mi cuenta',
                        'question1': '3.1. ¿Cómo recuperar mi contraseña?',
                        'answer1': 'answer1',
                        'question2': '3.2. ¿Dónde verifico mis transferencias?',
                        'answer2': 'answer2',
                        'question3': '3.3. ¿Cómo suspendo mi cuenta por pérdida o robo del equipo?',
                        'answer3': 'answer3'
                    },
                    'accordion4': {
                        'title': 'Pagos, retiros y comisiones',
                        'question1': '4.1. ¿Debo pagar por inscripción?',
                        'answer1': 'answer1',
                        'question2': '4.2. ¿Quién asume el pago de comisión?',
                        'answer2': 'answer2',
                        'question3': '4.3. ¿Cómo se aplica la comisión?',
                        'answer3': 'answer3',
                        'question4': '4.4. ¿Por qué medios puedo compartir mi enlace o QR?',
                        'answer4': 'answer4',
                        'question5': '4.5. ¿Qué tengo que hacer para pagar cuando recibo un link o QR?',
                        'answer5': 'answer5',
                        'question6': '4.6. ¿En cuánto tiempo llega el dinero que transferí?',
                        'answer6': 'answer6',
                        'question7': '4.7. ¿Cómo sé si el dinero ingresó a mi tarjeta?',
                        'answer7': 'answer7',
                        'question8': '4.8. ¿Cuándo puedo retirar mi dinero?',
                        'answer8': 'answer8',
                        'question9': '4.9. ¿Cómo puedo retirar mi dinero?',
                        'answer9': 'answer9'
                    },
                    'accordion5': {
                        'title': 'Seguridad y Fraudes',
                        'question1': '5.1. ¿Cuáles son sus niveles de seguridad?',
                        'answer1': 'answer1',
                        'question2': '5.2. ¿Qué hacer ante un enlace fraudulento?',
                        'answer2': 'answer2',
                        'question3': '5.3. ¿Cómo identifico un link oficial de paga.pe?',
                        'answer3': 'answer3'
                    }
                },

                'footer': {
                    'policy': 'Políticas de Privacidad',
                    'terms': 'Términos y Condiciones',
                    'book': 'Libro de Reclamaciones',
                    'copy': '2020, Olympus Systems. Todos los derechos reservados.',
                    'address': 'Av. Santo Toribio 173 - Torre Real 8 Piso 16, San Isidro Lima - Peru',
                }
            }
        }
    }
}, function (err, t) {
    // Nav
    document.getElementById('pp-nav-home').innerHTML = i18next.t('nav-home');
    // document.getElementById('pp-nav-about-us').innerHTML = i18next.t('nav-about-us');
    document.getElementById('pp-nav-pricing').innerHTML = i18next.t('nav-pricing');
    document.getElementById('pp-nav-contact').innerHTML = i18next.t('nav-contact');
    document.getElementById('pp-nav-pros').innerHTML = i18next.t('nav-pros');
    document.getElementById('pp-nav-register').innerHTML = i18next.t('nav-register');

    document.getElementById('pp-button-login').innerHTML = i18next.t('button-login');
    document.getElementById('pp-button-login').onclick = function () { singIn() };
    document.getElementById('pp-button-register').innerHTML = i18next.t('button-register');
    document.getElementById('pp-button-register').onclick = function () { register() };

    // Home Template


    homeTemplate.content.querySelector('#pp-home-title').innerHTML = i18next.t('home.title');
    homeTemplate.content.querySelector('#pp-home-subtitle1').innerHTML = i18next.t('home.subtitle1');
    homeTemplate.content.querySelector('#pp-home-subtitle2').innerHTML = i18next.t('home.subtitle2');
    homeTemplate.content.querySelector('#pp-home-itemtitle').innerHTML = i18next.t('home.itemtitle');
    homeTemplate.content.querySelector('#pp-home-item1-1').innerHTML = i18next.t('home.item1-1');
    homeTemplate.content.querySelector('#pp-home-item1-2').innerHTML = i18next.t('home.item1-2');
    homeTemplate.content.querySelector('#pp-home-item2-1').innerHTML = i18next.t('home.item2-1');
    homeTemplate.content.querySelector('#pp-home-item2-2').innerHTML = i18next.t('home.item2-2');
    homeTemplate.content.querySelector('#pp-home-item3-1').innerHTML = i18next.t('home.item3-1');
    homeTemplate.content.querySelector('#pp-home-item3-2').innerHTML = i18next.t('home.item3-2');

    homeTemplate.content.querySelector('#pp-home-footer-title1').innerHTML = i18next.t('home.footer.title1');
    homeTemplate.content.querySelector('#pp-home-footer-title2').innerHTML = i18next.t('home.footer.title2');

    homeTemplate.content.querySelector('#pp-button-start-1').innerHTML = i18next.t('button-start');

    homeTemplate.content.querySelector('#pp-button-freetrial').innerHTML = i18next.t('button-freetrial');

    homeTemplate.content.querySelector('#pp-home-movil-pagape1').innerHTML = i18next.t('home.movil.pagape');
    homeTemplate.content.querySelector('#pp-home-movil-ago1').innerHTML = i18next.t('home.movil.ago1');
    homeTemplate.content.querySelector('#pp-home-movil-msg1').innerHTML = i18next.t('home.movil.msg1');

    homeTemplate.content.querySelector('#pp-home-movil-pagape2').innerHTML = i18next.t('home.movil.pagape');
    homeTemplate.content.querySelector('#pp-home-movil-ago2').innerHTML = i18next.t('home.movil.ago2');
    homeTemplate.content.querySelector('#pp-home-movil-msg2').innerHTML = i18next.t('home.movil.msg2');

    // Pro
    homeTemplate.content.querySelector('#pp-pro-title').innerHTML = i18next.t('pro.title');
    homeTemplate.content.querySelector('#pp-pro-subtitle').innerHTML = i18next.t('pro.subtitle');

    homeTemplate.content.querySelector('#pp-pro-step1-title1').innerHTML = i18next.t('pro.step1.title1');
    homeTemplate.content.querySelector('#pp-pro-step1-detail2').innerHTML = i18next.t('pro.step1.detail2');
    homeTemplate.content.querySelector('#pp-pro-step1-detail3').innerHTML = i18next.t('pro.step1.detail3');

    homeTemplate.content.querySelector('#pp-pro-step2-title1').innerHTML = i18next.t('pro.step2.title1');
    homeTemplate.content.querySelector('#pp-pro-step2-detail2').innerHTML = i18next.t('pro.step2.detail2');
    homeTemplate.content.querySelector('#pp-pro-step2-detail3').innerHTML = i18next.t('pro.step2.detail3');

    homeTemplate.content.querySelector('#pp-pro-step3-title1').innerHTML = i18next.t('pro.step3.title1');
    homeTemplate.content.querySelector('#pp-pro-step3-detail2').innerHTML = i18next.t('pro.step3.detail2');
    homeTemplate.content.querySelector('#pp-pro-step3-detail3').innerHTML = i18next.t('pro.step3.detail3');

    // About us
    homeTemplate.content.querySelector('#pp-about-title').innerHTML = i18next.t('about.title');
    homeTemplate.content.querySelector('#pp-about-subtitle').innerHTML = i18next.t('about.subtitle');
    homeTemplate.content.querySelector('#pp-about-text1').innerHTML = i18next.t('about.text1');
    homeTemplate.content.querySelector('#pp-about-text2').innerHTML = i18next.t('about.text2');
    homeTemplate.content.querySelector('#pp-about-text3').innerHTML = i18next.t('about.text3');
    homeTemplate.content.querySelector('#pp-about-text4').innerHTML = i18next.t('about.text4');
    homeTemplate.content.querySelector('#pp-about-detail1').innerHTML = i18next.t('about.detail1');

    // document.getElementById('pp-about-subtitle-2').innerHTML = i18next.t('about.subtitle2');

    // Register
    homeTemplate.content.querySelector('#pp-register-title').innerHTML = i18next.t('register.title');
    homeTemplate.content.querySelector('#pp-register-subtitle').innerHTML = i18next.t('register.subtitle');

    homeTemplate.content.querySelector('#pp-register-step1-title1').innerHTML = i18next.t('register.step1.title1');
    homeTemplate.content.querySelector('#pp-register-step1-detail1').innerHTML = i18next.t('register.step1.detail1');

    homeTemplate.content.querySelector('#pp-register-step2-title1').innerHTML = i18next.t('register.step2.title1');
    homeTemplate.content.querySelector('#pp-register-step2-detail1').innerHTML = i18next.t('register.step2.detail1');

    homeTemplate.content.querySelector('#pp-register-step3-title1').innerHTML = i18next.t('register.step3.title1');
    homeTemplate.content.querySelector('#pp-register-step3-detail1').innerHTML = i18next.t('register.step3.detail1');

    // Pricing
    homeTemplate.content.querySelector('#pp-pricing-title').innerHTML = i18next.t('pricing.title');
    homeTemplate.content.querySelector('#pp-pricing-subtitle').innerHTML = i18next.t('pricing.subtitle');

    homeTemplate.content.querySelector('#pp-pricing-item1-detail1').innerHTML = i18next.t('pricing.item1.detail1');
    homeTemplate.content.querySelector('#pp-pricing-item1-detail2').innerHTML = i18next.t('pricing.item1.detail2');
    homeTemplate.content.querySelector('#pp-pricing-item1-detail3').innerHTML = i18next.t('pricing.item1.detail3');
    homeTemplate.content.querySelector('#pp-pricing-item1-detail4').innerHTML = i18next.t('pricing.item1.detail4');

    homeTemplate.content.querySelector('#pp-pricing-item2-detail1').innerHTML = i18next.t('pricing.item2.detail1');
    homeTemplate.content.querySelector('#pp-pricing-item2-detail2').innerHTML = i18next.t('pricing.item2.detail2');
    homeTemplate.content.querySelector('#pp-pricing-item2-detail3').innerHTML = i18next.t('pricing.item2.detail3');
    homeTemplate.content.querySelector('#pp-pricing-item2-detail4').innerHTML = i18next.t('pricing.item2.detail4');

    homeTemplate.content.querySelector('#pp-pricing-item3-detail1').innerHTML = i18next.t('pricing.item3.detail1');
    homeTemplate.content.querySelector('#pp-pricing-item3-detail2').innerHTML = i18next.t('pricing.item3.detail2');

    homeTemplate.content.querySelector('#pp-pricing-item1-title').innerHTML = i18next.t('pricing.item1.title');
    homeTemplate.content.querySelector('#pp-pricing-item1-1').innerHTML = i18next.t('pricing.item1.subitem1');
    homeTemplate.content.querySelector('#pp-pricing-item1-2').innerHTML = i18next.t('pricing.item1.subitem2');
    homeTemplate.content.querySelector('#pp-pricing-item1-3').innerHTML = i18next.t('pricing.item1.subitem3');
    homeTemplate.content.querySelector('#pp-pricing-item1-4').innerHTML = i18next.t('pricing.item1.subitem4');
    homeTemplate.content.querySelector('#pp-pricing-item1-5').innerHTML = i18next.t('pricing.item1.subitem5');
    homeTemplate.content.querySelector('#pp-pricing-item1-6').innerHTML = i18next.t('pricing.item1.subitem6');

    homeTemplate.content.querySelector('#pp-pricing-item2-title').innerHTML = i18next.t('pricing.item2.title');
    homeTemplate.content.querySelector('#pp-pricing-item2-1').innerHTML = i18next.t('pricing.item2.subitem1');
    homeTemplate.content.querySelector('#pp-pricing-item2-2').innerHTML = i18next.t('pricing.item2.subitem2');
    homeTemplate.content.querySelector('#pp-pricing-item2-3').innerHTML = i18next.t('pricing.item2.subitem3');
    homeTemplate.content.querySelector('#pp-pricing-item2-4').innerHTML = i18next.t('pricing.item2.subitem4');
    homeTemplate.content.querySelector('#pp-pricing-item2-5').innerHTML = i18next.t('pricing.item2.subitem5');

    homeTemplate.content.querySelector('#pp-pricing-item3-title').innerHTML = i18next.t('pricing.item3.title');
    homeTemplate.content.querySelector('#pp-pricing-item3-1').innerHTML = i18next.t('pricing.item3.subitem1');
    homeTemplate.content.querySelector('#pp-pricing-item3-2').innerHTML = i18next.t('pricing.item3.subitem2');
    homeTemplate.content.querySelector('#pp-pricing-item3-3').innerHTML = i18next.t('pricing.item3.subitem3');
    homeTemplate.content.querySelector('#pp-pricing-item3-4').innerHTML = i18next.t('pricing.item3.subitem4');

    // Contact
    homeTemplate.content.querySelector('#pp-contact-title').innerHTML = i18next.t('contact.title');
    homeTemplate.content.querySelector('#pp-contact-subtitle').innerHTML = i18next.t('contact.subtitle');

    homeTemplate.content.querySelector('#pp-contact-item2-title1').innerHTML = i18next.t('contact.item2.title1');
    homeTemplate.content.querySelector('#pp-contact-item3-title1').innerHTML = i18next.t('contact.item3.title1');
    homeTemplate.content.querySelector('#pp-contact-item3-detail1').innerHTML = i18next.t('contact.item3.detail1');
    homeTemplate.content.querySelector('#pp-contact-item3-title2').innerHTML = i18next.t('contact.item3.title2');

    // faq
    faqTemplate.content.querySelector('#pp-faq-accordion1-title').innerHTML = i18next.t('faq.accordion1.title');
    faqTemplate.content.querySelector('#pp-faq-accordion1-question1').innerHTML = i18next.t('faq.accordion1.question1');
    faqTemplate.content.querySelector('#pp-faq-accordion1-answer1').innerHTML = i18next.t('faq.accordion1.answer1');
    faqTemplate.content.querySelector('#pp-faq-accordion1-question2').innerHTML = i18next.t('faq.accordion1.question2');
    faqTemplate.content.querySelector('#pp-faq-accordion1-answer2').innerHTML = i18next.t('faq.accordion1.answer2');

    faqTemplate.content.querySelector('#pp-faq-accordion2-title').innerHTML = i18next.t('faq.accordion2.title');
    faqTemplate.content.querySelector('#pp-faq-accordion2-question1').innerHTML = i18next.t('faq.accordion2.question1');
    faqTemplate.content.querySelector('#pp-faq-accordion2-answer1').innerHTML = i18next.t('faq.accordion2.answer1');
    faqTemplate.content.querySelector('#pp-faq-accordion2-question2').innerHTML = i18next.t('faq.accordion2.question2');
    faqTemplate.content.querySelector('#pp-faq-accordion2-answer2').innerHTML = i18next.t('faq.accordion2.answer2');

    faqTemplate.content.querySelector('#pp-faq-accordion3-title').innerHTML = i18next.t('faq.accordion3.title');
    faqTemplate.content.querySelector('#pp-faq-accordion3-question1').innerHTML = i18next.t('faq.accordion3.question1');
    faqTemplate.content.querySelector('#pp-faq-accordion3-answer1').innerHTML = i18next.t('faq.accordion3.answer1');
    faqTemplate.content.querySelector('#pp-faq-accordion3-question2').innerHTML = i18next.t('faq.accordion3.question2');
    faqTemplate.content.querySelector('#pp-faq-accordion3-answer2').innerHTML = i18next.t('faq.accordion3.answer2');
    faqTemplate.content.querySelector('#pp-faq-accordion3-question3').innerHTML = i18next.t('faq.accordion3.question3');
    faqTemplate.content.querySelector('#pp-faq-accordion3-answer3').innerHTML = i18next.t('faq.accordion3.answer3');

    faqTemplate.content.querySelector('#pp-faq-accordion4-title').innerHTML = i18next.t('faq.accordion4.title');
    faqTemplate.content.querySelector('#pp-faq-accordion4-question1').innerHTML = i18next.t('faq.accordion4.question1');
    faqTemplate.content.querySelector('#pp-faq-accordion4-answer1').innerHTML = i18next.t('faq.accordion4.answer1');
    faqTemplate.content.querySelector('#pp-faq-accordion4-question2').innerHTML = i18next.t('faq.accordion4.question2');
    faqTemplate.content.querySelector('#pp-faq-accordion4-answer2').innerHTML = i18next.t('faq.accordion4.answer2');
    faqTemplate.content.querySelector('#pp-faq-accordion4-question3').innerHTML = i18next.t('faq.accordion4.question3');
    faqTemplate.content.querySelector('#pp-faq-accordion4-answer3').innerHTML = i18next.t('faq.accordion4.answer3');
    faqTemplate.content.querySelector('#pp-faq-accordion4-question4').innerHTML = i18next.t('faq.accordion4.question4');
    faqTemplate.content.querySelector('#pp-faq-accordion4-answer4').innerHTML = i18next.t('faq.accordion4.answer4');
    faqTemplate.content.querySelector('#pp-faq-accordion4-question5').innerHTML = i18next.t('faq.accordion4.question5');
    faqTemplate.content.querySelector('#pp-faq-accordion4-answer5').innerHTML = i18next.t('faq.accordion4.answer5');
    faqTemplate.content.querySelector('#pp-faq-accordion4-question6').innerHTML = i18next.t('faq.accordion4.question6');
    faqTemplate.content.querySelector('#pp-faq-accordion4-answer6').innerHTML = i18next.t('faq.accordion4.answer6');
    faqTemplate.content.querySelector('#pp-faq-accordion4-question7').innerHTML = i18next.t('faq.accordion4.question7');
    faqTemplate.content.querySelector('#pp-faq-accordion4-answer7').innerHTML = i18next.t('faq.accordion4.answer7');
    faqTemplate.content.querySelector('#pp-faq-accordion4-question8').innerHTML = i18next.t('faq.accordion4.question8');
    faqTemplate.content.querySelector('#pp-faq-accordion4-answer8').innerHTML = i18next.t('faq.accordion4.answer8');
    faqTemplate.content.querySelector('#pp-faq-accordion4-question9').innerHTML = i18next.t('faq.accordion4.question9');
    faqTemplate.content.querySelector('#pp-faq-accordion4-answer9').innerHTML = i18next.t('faq.accordion4.answer9');

    faqTemplate.content.querySelector('#pp-faq-accordion5-title').innerHTML = i18next.t('faq.accordion5.title');
    faqTemplate.content.querySelector('#pp-faq-accordion5-question1').innerHTML = i18next.t('faq.accordion5.question1');
    faqTemplate.content.querySelector('#pp-faq-accordion5-answer1').innerHTML = i18next.t('faq.accordion5.answer1');
    faqTemplate.content.querySelector('#pp-faq-accordion5-question2').innerHTML = i18next.t('faq.accordion5.question2');
    faqTemplate.content.querySelector('#pp-faq-accordion5-answer2').innerHTML = i18next.t('faq.accordion5.answer2');
    faqTemplate.content.querySelector('#pp-faq-accordion5-question3').innerHTML = i18next.t('faq.accordion5.question3');
    faqTemplate.content.querySelector('#pp-faq-accordion5-answer3').innerHTML = i18next.t('faq.accordion5.answer3');

    // Footer
    document.getElementById('pp-footer-copy').innerHTML = i18next.t('footer.copy');
    document.getElementById('pp-footer-address').innerHTML = i18next.t('footer.address');
    document.getElementById('pp-footer-policy').innerHTML = i18next.t('footer.policy');
    document.getElementById('pp-footer-terms').innerHTML = i18next.t('footer.terms');
    document.getElementById('pp-footer-book').innerHTML = i18next.t('footer.book');
});

function singIn() {
    location.href = portalSignIn;
}

function register() {
    location.href = portalRegister;
}

// function register() {
//     location.href = portalRegister;
// }

let ppSeti18Text = function (id, key) {
    let element = document.getElementById(id);
    if (!element) {
        console.error('i18, element id %s does not exist', id);
        return;
    }
    if (!i18next.exists(key)) {
        console.error('i18, key %s does not exist', key);
        return;
    }
    element.innerHTML = i18next.t(key);
}

var popupcontainer = document.getElementById("myModal");
// Get the <span> element that closes the modal
// var popupclose = document.getElementsByClassName("popupclose")[0];
// When the user clicks on <span> (x), close the modal

if (showModal) {
    popupcontainer.style.display = "block";
    portalSignIn = "#";
    portalRegister = "#";
}
// popupclose.onclick = function() {
//     popupcontainer.style.display = "none";
// }

// AOS effects
AOS.init();

// On DOM loaded
document.addEventListener('DOMContentLoaded', () => {
    // load images
    document.getElementById('pp-img-logo').src = logoImage;
    homeTemplate.content.querySelector('#pp-img-logo-movil1').src = logoMovil;
    homeTemplate.content.querySelector('#pp-img-logo-movil2').src = logoMovil;
    homeTemplate.content.querySelector('#pp-img-logo-white-1').src = logoImageWhite;
    homeTemplate.content.querySelector('#pp-img-logo-white-2').src = logoImageWhite;
    homeTemplate.content.querySelector('#pp-img-logo-grey1').src = logoImageGrey;
    homeTemplate.content.querySelector('#pp-img-logo-grey2').src = logoImageGrey;
    // homeTemplate.content.querySelector('#pp-img-logo-square').src = logoSquare;
    homeTemplate.content.querySelector('#pp-img-logo-olympus-grey1').src = logoOlympusGrey;
    homeTemplate.content.querySelector('#pp-img-logo-olympus-grey2').src = logoOlympusGrey;
    homeTemplate.content.querySelector('#pp-img-logo-customer').src = logoCustomer;

    // Register images
    homeTemplate.content.querySelector('#pp-register-step1-img1').src = registerStep1Img1;
    homeTemplate.content.querySelector('#pp-register-step1-img2').src = registerStep1Img2;
    homeTemplate.content.querySelector('#pp-register-step1-img3').src = registerStep1Img3;
    homeTemplate.content.querySelector('#pp-register-step1-num1').src = registerStep1Num1;

    homeTemplate.content.querySelector('#pp-register-step2-img1').src = registerStep2Img1;
    homeTemplate.content.querySelector('#pp-register-step2-img2').src = registerStep2Img2;
    homeTemplate.content.querySelector('#pp-register-step2-img3').src = registerStep2Img3;
    homeTemplate.content.querySelector('#pp-register-step2-num1').src = registerStep2Num1;

    homeTemplate.content.querySelector('#pp-register-step3-img1').src = registerStep3Img1;
    homeTemplate.content.querySelector('#pp-register-step3-img2').src = registerStep3Img2;
    homeTemplate.content.querySelector('#pp-register-step3-img3').src = registerStep3Img3;
    homeTemplate.content.querySelector('#pp-register-step3-num1').src = registerStep3Num1;

    // contact images
    homeTemplate.content.querySelector('#pp-contact-award1').src = contactAward1;
    homeTemplate.content.querySelector('#pp-contact-award2').src = contactAward2;

    homeTemplate.content.querySelector('#pp-contact-item1-img1').src = contactItem1Img1;
    homeTemplate.content.querySelector('#pp-contact-item1-img2').src = contactItem1Img2;
    homeTemplate.content.querySelector('#pp-contact-item1-img3').src = contactItem1Img3;
    homeTemplate.content.querySelector('#pp-contact-item1-img4').src = contactItem1Img4;

    homeTemplate.content.querySelector('#pp-about-img1').src = aboutImg1;

    // faq
    faqTemplate.content.querySelector('#pp-faq-img01').src = faqImg1;
    faqTemplate.content.querySelector('#pp-faq-img02').src = faqImg2;
    faqTemplate.content.querySelector('#pp-faq-img03').src = faqImg3;
    faqTemplate.content.querySelector('#pp-faq-img04').src = faqImg4;
    faqTemplate.content.querySelector('#pp-faq-img05').src = faqImg5;

    document.getElementById('pp-popup-img').src = popupImg;

    // clock
    setInterval(function () {
        if (document.getElementById('pp-home-clock-hhmm')) {
            document.getElementById('pp-home-clock-hhmm').innerText = Moment().format('h:mm');
            document.getElementById('pp-home-clock-day').innerText = Moment().format('dddd, MMMM D');
        }
    }, 1000);
    
    //
    let mainNavLinks = document.querySelectorAll(".navbar-start .navbar-item");

    window.addEventListener("scroll", () => {
        let navHeight = document.getElementById('pp-navbar').offsetHeight;
        let fromTop = window.scrollY;

        mainNavLinks.forEach(link => {

            // if (!link.hash || link.hash.substring(1, 2) ==='/'){
            //     return;
            // }

            if (!link.hash) {
                return;
            }

            let section;

            if (link.hash === '#/faq') {
                section = document.querySelector('#pp-faq-section');
            } else {
                section = document.querySelector(link.hash);
            }

            if (!section) {
                return;
            }
            // if (
            //     (section.offsetTop - navHeight) <= fromTop &&
            //     (section.offsetTop - navHeight) + section.offsetHeight > fromTop
            // ) {
            //     link.classList.add("is-active");
            // } else {
            //     link.classList.remove("is-active");
            // }
        });
    });


    // generic nav burgers
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    if ($navbarBurgers.length > 0) {
        $navbarBurgers.forEach(el => {
            el.addEventListener('click', () => {
                const target = el.dataset.target;
                const $target = document.getElementById(target);
                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');
            });
        });
    }
});