const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const path = require("path");

module.exports = {
    module: {
        rules: [{
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            // options...
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader',
                ],
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            favicon: './src/images/favicon.ico',
            template: path.resolve(__dirname, "src", "index.html")
        }),
        new MiniCssExtractPlugin()
    ]
};

// module.exports = {
//     module: {
//         rules: [{
//                 test: /\.scss$/,
//                 use: [
//                     MiniCssExtractPlugin.loader,
//                     {
//                         loader: 'css-loader'
//                     },
//                     {
//                         loader: 'sass-loader',
//                         options: {
//                             sourceMap: true,
//                             // options...
//                         }
//                     }
//                 ]
//             },
//             {
//                 test: /\.(png|jp(e*)g|svg)$/,
//                 use: [{
//                     loader: 'url-loader',
//                     options: {
//                         limit: 8000,
//                         // publicPath: 'assets',
//                         // name: 'images/[hash]-[name].[ext]'
//                     }
//                 }]
//             },
//             // {
//             //     test: /favicon\.ico$/,
//             //     use: 'file-loader?name=[name].[ext]',
//             // },
//         ]
//     },
//     optimization: {
//         splitChunks: { chunks: "all" }
//     },
//     plugins: [
//         new MiniCssExtractPlugin({
//             filename: 'css/style.css'
//         }),
//         new HtmlWebpackPlugin({
//             favicon: './src/favicon.ico',
//             template: path.resolve(__dirname, "src", "index.html")
//         })
//     ]
// };